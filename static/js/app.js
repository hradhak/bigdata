/**
 * Created by PyCharm.
 * User: hari
 * Date: 2/7/12
 * Time: 8:15 AM
 * To change this template use File | Settings | File Templates.
 */

Gen = Ember.Application.create();

Gen.Transfusion = Ember.Object.extend({
   type:null,
   dt:null
});

Gen.ModeTransportController = Ember.ArrayController.create({
    content: [
        {id: 'air', result:'Air'},
        {id: 'ground', result:'Ground'}
    ]
});


Gen.Demographics = Em.Object.extend({
    hospital_arrival : null,
    current_time: null,
    moi : null,
    trauma_code : null,
    age : null,
    trauma: null,
    mode_transport : Gen.ModeTransportController.content[0],
    demo2_class: "demo2",

    set_demo: function(key) {
        var val = this.get(key)
        var d = {};
        d[key] = val;
        $.ajax({
            url:'/set_demographics/',
            data:d,
            type:'POST',
        });
    },

    tc: function() {
        var trauma_code = this.get('trauma_code');
        if (trauma_code == '2U') return "Code 2 Upgrade";
        else if (trauma_code >= 1 && trauma_code <= 3) return "Code " + trauma_code;
        else return "Unknown";
    }.property('trauma_code'),

    mechanism: function() {
        var moi = this.get("moi");
        if (moi == 'B') return "Blunt";
        else if (moi == 'P') return "Penetrating";
        else return "Unknown";

    }.property('moi'),
    ageClean: function() {
        var age = this.get('age');
        if (age>13 && age<120) return age;
        else return "Unknown";

    }.property('age'),

    moiChanged: function() {
        this.set_demo('moi');

    }.observes('moi'),

    trauma_codeChanged: function() {
        this.set_demo('trauma_code');
    }.observes('trauma_code'),

    ageChanged: function() {
        this.set_demo('age');
    }.observes('age'),

    mode_transportChanged: function() {
        this.set_demo('mode_transport');
    }.observes('mode_transport'),

    hospital_los: function() {
        var hospital_arrival = Math.round(this.get('hospital_arrival') /1000);
        var current_time = Math.round((new Date()).getTime() / 1000);
        var x = current_time - hospital_arrival;
        var hours = Math.floor(x/3600);
        var minutes = Math.floor((x-(hours*3600))/60);
        if (minutes<10) minutes = '0'+minutes;
        var seconds = (x-((hours*3600)+(minutes*60)));
        if (seconds<10) seconds = '0'+seconds;
        var timeformatted = hours+":"+minutes+"."+seconds;
        return timeformatted;

    }.property('hospital_arrival','current_time')
});

Gen.PatientController = Em.Object.create({
    demographics:Gen.Demographics.create(),


})
Gen.DemographicsView = Em.View.extend({
   tcBinding : Ember.Binding.oneWay('Gen.PatientController.demographics.tc'),
   mechanismBinding : Ember.Binding.oneWay('Gen.PatientController.demographics.mechanism'),
   ageCleanBinding : Ember.Binding.oneWay('Gen.PatientController.demographics.ageClean'),
   hospital_losBinding : Ember.Binding.oneWay('Gen.PatientController.demographics.hospital_los')
});



Gen.Vitals = Em.Object.create({
    hr : null,
    sbp : null,
    dbp : null,
    sick: false,





    sickChanged: function() {
        var sick = this.get('sick');
        $.getJSON('/sick/',{sick:sick});
    }.observes('sick'),

    sick_patient: function() {
        var sick = this.get('sick')
        this.set('sick',!(sick))
        return null;
    }
})

Gen.FastController = Ember.ArrayController.create({
    content: [
        {id: 'nd', result:'Not Done'},
        {id: 'u', result:'Unknown'},
        {id: 'p', result:'Positive'},
        {id: 'n', result:'Negative'}

    ]
});

Gen.Fast = Em.Object.create({
    fast_exam : Gen.FastController.content[0],

    fast_examChanged: function(sender,key,value,context,rev) {
        //console.log(sender,key,value,context,rev);
        var fast_exam = this.get('fast_exam');
        //console.log(fast_exam);
        $.getJSON('/fast/',{id:fast_exam.id,result:fast_exam.result});
    }.observes('fast_exam')


});

Gen.Labs = Em.Object.extend({
   type: null,
   timestamp:null,
   value:null
}),

Gen.Rads = Em.Object.extend({
    type:null,
    timestamp:null,
    value:null
})

Gen.RadsController = Em.ArrayProxy.create({
    content: [

    ],

    get_unique : function() {
        var foo = this.mapProperty('type');
        var o = {}, i, l = foo.length, r = [];
        for(i=0; i<l;i+=1) o[foo[i]] = foo[i];
        for(i in o) r.push(o[i]);
        return r;

    },

    latest: function() {

        var foo = this.mapProperty('timestamp');

        if (foo.length == 0) return 0;
        else return foo[foo.length-1];
        //return this.filterProperty('timestamp').get('timestamp');
    }.property('@each.timestamp'),

    results: function() {
        //console.log("Results fn called");
        var foo = this.get_unique();

        var out = Ember.ArrayProxy.create({content:[]});
        for (var i = 0; i < foo.length;i++) {
            var result_type = {type:foo[i]}

            var results = []
            var rads = this.filterProperty('type',foo[i]);

            for (var j=0;j < rads.length;j++) {

                results.push({val:rads[j].value,timestamp:human_time_diff(Gen.PatientController.demographics.hospital_arrival,rads[j].timestamp)})
            }
            result_type['results'] = results
            //console.log(result_type);
            out.pushObject(result_type);
        }
        //console.log(out.content);
        return out;
    }.property('@each.type').cacheable(),

    add_results: function(type,timestamp,val,sync) {
        if (sync == undefined) sync = false;

        if (val == null) return null
        var foo = {type:type,timestamp:timestamp,value:val}
        var rads_result = Gen.Rads.create(foo);
        this.pushObject(rads_result);

        console.log("Sending to server...");
        if (sync == true) $.post('/rads/',foo);

    }
    
})


Gen.LabsController = Em.ArrayProxy.create({
    content : [
    ],

    get_unique : function() {
        var foo = this.mapProperty('type');
        var o = {}, i, l = foo.length, r = [];
        for(i=0; i<l;i+=1) o[foo[i]] = foo[i];
        for(i in o) r.push(o[i]);
        return r;

    },

    latest: function() {

        var foo = this.mapProperty('timestamp');

        if (foo.length == 0) return 0;
        else return foo[foo.length-1];
        //return this.filterProperty('timestamp').get('timestamp');
    }.property('@each.timestamp'),

    results: function() {
        //console.log("Results fn called");
        var foo = this.get_unique();

        var out = Ember.ArrayProxy.create({content:[]});
        for (var i = 0; i < foo.length;i++) {
            var result_type = {type:foo[i]}
            
            var results = []
            var labs = this.filterProperty('type',foo[i]);

            for (var j=0;j < labs.length;j++) {

                results.push({val:labs[j].value,timestamp:human_time_diff(Gen.PatientController.demographics.hospital_arrival,labs[j].timestamp)})
            }
            result_type['results'] = results
            //console.log(result_type);
            out.pushObject(result_type);
        }
        //console.log(out.content);
        return out;
    }.property('@each.type').cacheable(),

    add_results: function(type,timestamp,val,sync) {
        if (sync == undefined) sync = false;

        if (val == null) return null
        var foo = {type:type,timestamp:timestamp,value:val}
        var lab_result = Gen.Labs.create(foo);
        this.pushObject(lab_result);

        console.log("Sending to server...");
        if (sync == true) $.post('/labs/',foo);

    }
})

Gen.LabsView = Em.CollectionView.extend({
    contentBinding : 'Gen.LabsController.results',
    demographicsBinding : 'Gen.PatientController.demographics',
})

Gen.RadsAddResultsView = Em.TextField.extend({
  result_type : null,
  insertNewline: function() {
    var value = this.get('value');
    
    var result_type = this.get('result_type');
    if (value) {
      var x = new Date().getTime();
      Gen.RadsController.add_results(result_type,x,value,true);
      this.set('value','');
    }
  }
});

Gen.LabAddResultsView = Em.TextField.extend({
  result_type : null,
  insertNewline: function() {
    var value = this.get('value');

    var result_type = this.get('result_type');
    if (value) {
      var x = new Date().getTime();
      Gen.LabsController.add_results(result_type,x,value,true);
      this.set('value','');
    }
  }
});

Gen.LSI = Em.Object.extend({
    type : null,
    timestamp : null,
})


Gen.LSIController = Em.ArrayProxy.create({
    content:[],

    rbc: function() {
        return this.filterProperty('type','rbc').get('length');
    }.property('@each.type'),

    ffp: function() {
        return this.filterProperty('type','ffp').get('length');
    }.property('@each.type'),

    plt: function() {
        return this.filterProperty('type','plt').get('length');
    }.property('@each.type'),

    pelvic_binder: function() {
        var foo = this.filterProperty('type','pelvic_binder');
        if (foo.length == 0) return null
        else return foo[foo.length-1].get('timestamp')
    }.property('@each.type'),

    needle: function() {
        var foo = this.filterProperty('type','needle');
        if (foo.length == 0) return null
        else return foo[foo.length-1].get('timestamp')
    }.property('@each.type'),

    thoracotomy : function() {
        var foo = this.filterProperty('type','thoracotomy');
        if (foo.length == 0) return null
        else return foo[foo.length-1].get('timestamp')
        //console.log(foo);
    }.property('@each.type'),

    cric : function() {
        var foo = this.filterProperty('type','cric');
        if (foo.length == 0) return null
        else return foo[foo.length-1].get('timestamp')
    }.property('@each.type'),

    tourniquet : function() {
        var foo = this.filterProperty('type','tourniquet');
        if (foo.length == 0) return null
        else return foo[foo.length-1].get('timestamp')
    }.property('@each.type'),

    intubation : function() {
        var foo = this.filterProperty('type','intubation');
        if (foo.length == 0) return null
        else return foo[foo.length-1].get('timestamp')
    }.property('@each.type'),

    chest_tube : function() {
        var foo = this.filterProperty('type','chest_tube');
        if (foo.length == 0) return null
        else return foo[foo.length-1].get('timestamp')
    }.property('@each.type'),

    cpr : function() {
        var foo = this.filterProperty('type','cpr');
        if (foo.length == 0) return null
        else return foo[foo.length-1].get('timestamp')
    }.property('@each.type'),

    cardioversion : function() {
        var foo = this.filterProperty('type','cardioversion');
        if (foo.length == 0) return null
        else return foo[foo.length-1].get('timestamp')
    }.property('@each.type'),


    lsi: function(type) {
        var x = new Date().getTime();
        //var unit = Gen.Unit.create({type:type,timestamp:x})
        //this.pushObject(unit);

        this.add_lsi(type,x);


        $.getJSON('/lsi/'+type+'/',{time:x});

    },

    add_lsi: function(type,timestamp) {
        var lsi = Gen.LSI.create({type:type,timestamp:timestamp})
        this.pushObject(lsi);
    },

    latest: function() {

        var foo = this.mapProperty('timestamp');

        if (foo.length == 0) return 0;
        else return foo[foo.length-1];
        //return this.filterProperty('timestamp').get('timestamp');
    }.property('@each.timestamp'),

    transfuse_rbc: function() {
        this.lsi('rbc');
    },
    transfuse_ffp: function() {
        this.lsi('ffp');
    },

    transfuse_plt: function() {
        this.lsi('plt');
    },

    lsi_pelvic_binder: function() {
        this.lsi('pelvic_binder');
    },

    lsi_needle: function() {
        this.lsi('needle');
    },

    lsi_thoracotomy: function() {
        this.lsi('thoracotomy');
    },
    lsi_cric: function() {
        this.lsi('cric');
    },
    lsi_tourniquet: function() {
        this.lsi('tourniquet');
    },
    lsi_intubation: function() {
        this.lsi('intubation');
    },
    lsi_chest_tube: function() {
        this.lsi('chest_tube');
    },
    lsi_cpr: function() {
        this.lsi('cpr');
    },
    lsi_cardioversion: function() {
        this.lsi('cardioversion');
    },


})
/*
Gen.Unit = Em.Object.create({
    rbc : 0,
    ffp : 0,
    plt : 0,

    transfuse: function(type) {
        var foo = this.get(type);
        foo += 1;
        var x = new Date().getTime();
        this.set(type,foo);
        $.getJSON('/transfuse/'+type+'/',{time:x});
        return null;
    },

    transfuse_rbc: function() {
        this.transfuse('rbc');
    },

    transfuse_ffp: function() {
        this.transfuse('ffp');
    },

    transfuse_plt: function() {
        this.transfuse('plt');
    }

});
*/



function human_time_diff(start,end) {
    if (start == null || end == null) return null;
    var x = (Math.round(end/1000) - Math.round(start/1000));




    /*var hospital_arrival = Math.round(this.get('hospital_arrival') /1000);
        var current_time = Math.round((new Date()).getTime() / 1000);
        var x = current_time - hospital_arrival;*/
        var hours = Math.floor(x/3600);
        var minutes = Math.floor((x-(hours*3600))/60);
        if (minutes<10) minutes = '0'+minutes;
        var seconds = (x-((hours*3600)+(minutes*60)));
        if (seconds<10) seconds = '0'+seconds;
        var timeformatted = hours+":"+minutes+"."+seconds;
        return timeformatted;

    /*var foo = x / 1000;
    foo = Math.round(foo*100) / 100;
    if (foo > 60 && foo < 3600) return foo / 60 + " minutes";
    else if (foo >= 3600 && foo < 86400) return foo / 3600 + " hours";
    else if (foo >= 86400) return foo / 86400 + " days";
    else return foo + " seconds";*/
}


Gen.LSIView = Em.View.extend({
    thoracotomyBinding : 'Gen.LSIController.thoracotomy',
    pelvic_binderBinding : 'Gen.LSIController.pelvic_binder',
    needleBinding : 'Gen.LSIController.needle',
    cricBinding : 'Gen.LSIController.cric',
    tourniquetBinding : 'Gen.LSIController.tourniquet',
    intubationBinding : 'Gen.LSIController.intubation',
    chest_tubeBinding : 'Gen.LSIController.chest_tube',
    cprBinding : 'Gen.LSIController.cpr',
    cardioversionBinding : 'Gen.LSIController.cardioversion',

    demographicsBinding : 'Gen.PatientController.demographics',

    thoracotomyTime : function() {
        var t = this.get('thoracotomy');
        var d = this.get('demographics');

        return human_time_diff(d.hospital_arrival,t);

    }.property('thoracotomy','demographics'),

    pelvic_binderTime : function() {
        var t = this.get('pelvic_binder');
        var d = this.get('demographics');

        return human_time_diff(d.hospital_arrival,t);

    }.property('pelvic_binder','demographics'),

    needleTime : function() {
        var t = this.get('needle');
        var d = this.get('demographics');

        return human_time_diff(d.hospital_arrival,t);

    }.property('needle','demographics'),

    cricTime : function() {
        var t = this.get('cric');
        var d = this.get('demographics');

        return human_time_diff(d.hospital_arrival,t);

    }.property('cric','demographics'),

    tourniquetTime : function() {
        var t = this.get('tourniquet');
        var d = this.get('demographics');

        return human_time_diff(d.hospital_arrival,t);

    }.property('tourniquet','demographics'),

    intubationTime : function() {
        var t = this.get('intubation');
        var d = this.get('demographics');

        return human_time_diff(d.hospital_arrival,t);

    }.property('pelvic_binder','demographics'),

    chest_tubeTime : function() {
        var t = this.get('chest_tube');
        var d = this.get('demographics');

        return human_time_diff(d.hospital_arrival,t);

    }.property('chest_tube','demographics'),

    cardioversionTime : function() {
        var t = this.get('cardioversion');
        var d = this.get('demographics');

        return human_time_diff(d.hospital_arrival,t);

    }.property('cardioversion','demographics'),
    cprTime : function() {
        var t = this.get('cpr');
        var d = this.get('demographics');

        return human_time_diff(d.hospital_arrival,t);

    }.property('cpr','demographics')




})

Gen.VitalsView = Em.View.extend({
    hrBinding : 'Gen.Vitals.hr',
    sbpBinding : 'Gen.Vitals.sbp',
    dbpBinding : 'Gen.Vitals.dbp',
    hr_class: function () {
        var hr = this.get('hr');
        //console.log(hr);
        if (hr < 60 || hr > 100) return "warning";
        else return "normal";
    }.property('hr'),

    bp: function() {
        var sbp = this.get('sbp');
        var dbp = this.get('dbp');

        return sbp + ' / ' + dbp;

    }.property('sbp','dbp'),

    bp_class: function () {
        var sbp = this.get('sbp');
        //console.log(sbp);
        if (sbp < 90 || sbp > 170) return "warning";
        else return "normal";
    }.property('sbp'),
});

Gen.FastView = Em.View.extend({
    fast_examBinding : 'Gen.Fast.fast_exam',
    fast_exam_class: function () {
        var fast = this.get('fast_exam');
        console.log("Fast is: "+fast.result);
        if (fast.result=="Positive") return "warning";
        else return "normal";

    }.property('fast_exam')
});
