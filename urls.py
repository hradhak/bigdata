from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'bigdata.views.home', name='home'),
    # url(r'^bigdata/', include('bigdata.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    #(r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/home/hari/code/bigdata/static/'}),
    (r'^graph/$','bigdata.generator.views.graph'),
    (r'^vitals/$','bigdata.generator.views.get_vitals'),
    (r'^set_vitals/$','bigdata.generator.views.set_vitals'),
    (r'^demographics/$','bigdata.generator.views.get_demographics'),
    (r'^set_demographics/$','bigdata.generator.views.set_demographics'),
    (r'^reset/$','bigdata.generator.views.reset'),
    (r'^control/$','bigdata.generator.views.control'),
    (r'^sick/$','bigdata.generator.views.set_sick'),
    (r'^fast/$','bigdata.generator.views.fast'),
    (r'^labs/$','bigdata.generator.views.labs'),
    (r'^rads/$','bigdata.generator.views.rads'),
    (r'^timestamp/$','bigdata.generator.views.timestamp'),
    (r'^lsi/$', 'bigdata.generator.views.lsi'),
    (r'^lsi/(?P<type>\w+)/$', 'bigdata.generator.views.lsi'),
    (r'^pdf/$', 'bigdata.generator.views.get_pdf'),
)


try:
    from local_urls import local_urls_patterns
    urlpatterns += local_urls_patterns
except:
    pass

